import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  success: boolean = false;
  spinner: boolean = false;
  constructor() {}

  ngOnInit(): void {}

  onSubmit() {
    this.spinner = true
    setTimeout(() => {
      console.log('here');
      this.success = true;
      this.spinner = false;
    }, 3000);
  }

}
